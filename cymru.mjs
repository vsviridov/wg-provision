import IpToAsn from "ip-to-asn";

const client = new IpToAsn();

export function cymruQuery(ips) {
  return new Promise((resolve, reject) => {
    client.query(ips, (err, data) => {
      if (err) {
        return reject(err);
      }

      resolve(data);
    });
  });
}
