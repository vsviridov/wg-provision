import { dirname, resolve } from "path";
import { readdir, readFile, writeFile, unlink } from "fs/promises";
import { v1 as uuid } from "uuid";
import { parse } from "ini";
import { promisify } from "util";
import { exec as _exec } from "child_process";
const exec = promisify(_exec);

import { pubKey } from "./wireguard.mjs";
import { VPN_INTERFACE } from "./config.mjs";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));
const invitesPath = resolve(__dirname, "invites");
const claimedPath = resolve(invitesPath, "claimed");

export async function getInvites() {
  const files = await readdir(invitesPath);

  const fullPathFiles = files.filter((file) => file !== "claimed");

  return Promise.all(fullPathFiles.map((file) => readInvite(file)));
}

export async function createInvite(invite_for, requested_by) {
  const invite_code = uuid();
  const timestamp = new Date().getTime();

  await writeFile(
    resolve(invitesPath, invite_code),
    JSON.stringify({ requested_by, invite_for, timestamp })
  );
}

export async function getClaimed() {
  const files = await readdir(claimedPath);

  return (
    await Promise.all(
      files.filter((x) => x !== ".gitkeep").map((file) => readClaimed(file))
    )
  ).reduce((acc, i) => {
    return { ...acc, [i.PublicKey]: i };
  }, {});
}

export async function readInvite(code) {
  const fullPath = resolve(invitesPath, code);

  const data = await readFile(fullPath, { encoding: "utf8" });

  const invite = {
    ...JSON.parse(data),
    invite_code: code,
  };

  return invite;
}

export async function readClaimed(code) {
  const fullPath = resolve(claimedPath, code);

  const data = await readFile(fullPath, { encoding: "utf8" });

  const parsed = parse(data);
  const comments = data
    .split(/[\r\n]/)
    .filter((line) => line.match(/^#/))
    .reduce((acc, comment) => {
      const [, key, value] = comment.match(/^# ([A-z]*) = (.*)$/);

      return { ...acc, [key]: value };
    }, {});

  const PublicKey = await pubKey(parsed.Interface.PrivateKey);

  return {
    ...parsed,
    ...comments,
    PublicKey,
  };
}

export async function removePeer(code) {
  const invite_path = resolve(claimedPath, code);

  const invite = await readFile(invite_path, { encoding: "ascii" }).then(parse);

  console.dir(invite);

  const privateKey = invite.Interface.PrivateKey;

  const peer = await pubKey(privateKey);

  await exec(`wg set ${VPN_INTERFACE} peer ${peer} remove`);
  await unlink(invite_path);
}

export async function removeRequest(code) {
  const invite_path = resolve(invitesPath, code);

  await unlink(invite_path);
}
