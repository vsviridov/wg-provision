import Koa from "koa";
import bodyParser from "koa-bodyparser";
import Views from "koa-ejs";
import error from "koa-error";
import logger from "koa-logger";
import koaStatic from "koa-static";
import { dirname, resolve } from "path";
import Router from "@koa/router";
import session from "koa-session";
import CSRF from "koa-csrf";
import qrCode from "qrcode";
import { fileURLToPath } from "url";
import { show, utils } from "wireguard-tools.js";
import ejs from "ejs";
import { promisify } from "util";
import dns from "dns";
import ip from "ip";
const { fromLong, toLong } = ip;

const reverse = promisify(dns.reverse);
const renderFile = promisify(ejs.renderFile);
const __dirname = dirname(fileURLToPath(import.meta.url));

import { readFile, writeFile, unlink } from "fs/promises";

import { addPeer } from "./wireguard.mjs";
import {
  checkMiddleware,
  applyMiddleware,
} from "./middlewares/admin.middleware.mjs";
import { enhanceViewsMiddleware } from "./middlewares/enhanceViews.middleware.mjs";
import {
  getInvites,
  getClaimed,
  createInvite,
  removePeer,
  readClaimed,
  removeRequest,
} from "./invites.mjs";

import { cymruQuery } from "./cymru.mjs";

const app = new Koa();
const router = new Router();
const viewsPath = resolve(__dirname, "views");
const invitesPath = resolve(__dirname, "invites");
const claimedPath = resolve(invitesPath, "claimed");

const inviteRegexp =
  /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i;

import * as config from "./config.mjs";
import { readFileSync } from "fs";
const keys = JSON.parse(readFileSync(resolve(__dirname, "keys.json")));

app.keys = keys;
app.proxy = true;

router.prefix(config.PREFIX);

app.use(error({ engine: "ejs", template: resolve(viewsPath, "_error.ejs") }));
app.use(logger());
app.use(koaStatic(resolve(__dirname, "public")));
app.use(session(app));

app.use(bodyParser({ strict: true }));
app.use(new CSRF());
app.use(checkMiddleware(config.iface));
app.use(enhanceViewsMiddleware());

Views(app, { root: viewsPath, viewExt: "ejs", cache: false, async: true });

router.get("/", async (ctx) => {
  const { invite_code } = ctx.query;
  if (invite_code && !invite_code.match(inviteRegexp)) {
    throw new Error("Invalid invite code: " + invite_code);
  }
  await ctx.render("index", {
    invite_code,
  });
});

router.get("/:code/claim", async (_ctx) => {});

router.post("/", async (ctx) => {
  const { invite_code } = ctx.request.body;

  if (!invite_code.match(inviteRegexp)) {
    throw new Error("Invalid invite code: " + invite_code);
  }

  let c;

  // Look up claimed info
  try {
    c = await readFile(resolve(claimedPath, invite_code), {
      encoding: "utf8",
    });
  } catch (ex) {
    //
  }
  const host = await reverse(config.gateway.address);
  const dns = process.env.DNS || config.iface.address;
  let invite_for, requested_by;

  if (!c) {
    try {
      const invite = await readFile(resolve(invitesPath, invite_code)).then(
        JSON.parse
      );

      invite_for = invite.invite_for;
      requested_by = invite.requested_by;
    } catch (ex) {
      throw new Error("Invalid invite code");
    }

    const { private: privKey, public: pubKey } = await utils.keygen();

    const { publicKey, portListen: listenPort, peers } = show(config.VPN_INTERFACE);

    const nextPeer = Object.keys(peers).length + 1;

    const address = fromLong(toLong(config.iface.address) + nextPeer);

    const data = {
      privKey,
      publicKey,
      listenPort,
      address,
      host,
      dns,
      invite_for,
      requested_by,
      invite_code,
    };
    const template = await renderFile(resolve(__dirname, "template.ejs"), data);

    if (!process.env.BYPASS) {
      await writeFile(resolve(claimedPath, invite_code), template);
      await unlink(resolve(invitesPath, invite_code));

      await addPeer(config.VPN_INTERFACE, pubKey, address);
    }

    c = template;
  }
  const configQR = await qrCode.toDataURL(c);

  await ctx.render("provision", { configQR, config: c, host, dns });
});

router.get("/invites", applyMiddleware(config.iface), async (ctx) => {
  const invites = await getInvites();

  await ctx.render("invite", { invites });
});

router.post("/invites", applyMiddleware(config.iface), async (ctx) => {
  const { invite_for } = ctx.request.body;
  const requested_by = ctx.request.ip;

  await createInvite(invite_for, requested_by);

  ctx.redirect(ctx.request.url);
});

router.get("/claimed", applyMiddleware(config.iface), async (ctx) => {
  const { peers } = show(config.VPN_INTERFACE);

  const ips = [
    ...new Set(
      Object.values(peers).map(
        (peer) => (peer.endpoint && peer.endpoint.split(":")[0]) || null
      )
    ),
  ];

  const asns = await cymruQuery(ips);

  const claimedInvites = await getClaimed();

  const allKeys = [
    ...new Set(Object.keys(peers).concat(Object.keys(claimedInvites))),
  ];

  const requesterMap = Object.values(claimedInvites).reduce(
    (acc, i) => ({ ...acc, [i.Interface.Address.split("/")[0]]: i.InviteFor }),
    { [config.HOST]: "System" }
  );

  const data = allKeys.reduce((acc, key) => {
    return {
      ...acc,
      [key]: {
        claimed: claimedInvites[key],
        peer: peers[key],
      },
    };
  }, {});

  await ctx.render("claimed", {
    data,
    requesterMap,
    asns,
    SystemIp: config.HOST,
  });
});

router.post("/remove_request", applyMiddleware(), async (ctx) => {
  console.dir(ctx.request.body);
  const { code } = ctx.request.body;

  await removeRequest(code);

  ctx.redirect(config.PREFIX + "invites");
});

router.post("/remove_peer", applyMiddleware(), async (ctx) => {
  const { invite_code } = ctx.request.body;

  removePeer(invite_code);

  ctx.redirect(config.PREFIX + "claimed");
});

router.post("/restore_peer", applyMiddleware(), async (ctx) => {
  const { invite_code } = ctx.request.body;

  const invite = await readClaimed(invite_code);

  console.dir(invite);

  ctx.redirect(config.PREFIX + "claimed");
});

app.use(router.routes()).use(router.allowedMethods());
app.listen(config.PORT, config.HOST, async function onListening() {
  const { address, port } = this.address();
  console.log(`Server listening on http://${address}:${port}/`);
  console.log(
    `Wireguard interface:\t${config.VPN_INTERFACE}\tAddress: ${config.iface.address}`
  );
  console.log(
    `Gateway interface:\t${config.GATEWAY_INTERFACE}\tAddress: ${config.gateway.address}`
  );
  console.log(`Prefix:\t\t\t${config.PREFIX}`);
  const host = await reverse(config.gateway.address);
  console.log(`External hostname:\t${host}`);
});
