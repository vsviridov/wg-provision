import ip from "ip";

const { cidrSubnet } = ip;

export const checkMiddleware = (iface) => (ctx, next) => {
  ctx.state.ip = ctx.request.ip;

  if (process.env.BYPASS) {
    ctx.state.bypass = true;
  }

  if (cidrSubnet(iface.cidr).contains(ctx.request.ip)) {
    ctx.state.admin = true;
  }

  return next();
};

export const applyMiddleware = () => (ctx, next) => {
  if (ctx.state.admin || ctx.state.bypass) {
    return next();
  }
};
