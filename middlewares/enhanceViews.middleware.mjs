import { PREFIX } from "../config.mjs";

import prettySize from "prettysize";
import { formatRelative, formatDistance } from "date-fns";

export function enhanceViewsMiddleware() {
  return (ctx, next) => {
    ctx.state.prefix = PREFIX;
    ctx.state.csrf = ctx.csrf;
    ctx.state.prettySize = prettySize;
    ctx.state.formatRelative = formatRelative;
    ctx.state.formatDistance = formatDistance;
    ctx.state.urlWithPrefix = (url = "") => PREFIX ? `${PREFIX}${url}` : url;
    return next();
  };
}
