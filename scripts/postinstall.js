#!/usr/bin/env node
const { resolve } = require("path");
const { v4: uuidv4 } = require("uuid");
const {
  existsSync,
  promises: { writeFile, mkdir },
} = require("fs");

const KEYS_PATH = resolve(process.env.PWD, "keys.json");

async function main() {
  await mkdir(resolve(process.env.PWD, "invites", "claimed"), {
    recursive: true,
  });
  if (existsSync(KEYS_PATH)) {
    return;
  }
  console.log("Generating application keys");
  const keys = Array.from({ length: 5 }).map(() => uuidv4());

  await writeFile(KEYS_PATH, JSON.stringify(keys));
}

main();
