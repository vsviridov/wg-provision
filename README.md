Simple WireGuard Provisioning Interface
=======================================

* Requires running under root, so likely unsafe. YMMV
* Should probably run from behind nginx over SSL
* Really dumb access control

Enironment variables
====================

* `PORT` controls listen port of the server. Default 3000. Use path if you want to bind to a unix socket instead
* `VPN_INTERFACE` name of the wireguard interface `wg0` by default. (Should already by present)
* `GATEWAY_INTERFACE` name of the gateway interface `eth0` by default. (Used for bootstrapping domain name of the VPN)
    * PTR record should be configured for it
* `DNS` address of the DNS server, I use local `unbound` (https://nlnetlabs.nl/projects/unbound/about/) attached to the VPN interface
* `PREFIX` url prefix for accessing the UI, `/wg/` by default. Upstream web server (e.g. nginx) should be configured for this prefix.

Installation
============

* `git clone git@gitlab.com:vsviridov/wg-provision /opt/wg-provision/current`
* `npm run postinstall`

Init Systems
============

### Systemd
* `ln -s /opt/wg-provision/current/init/wg-provision.service /lib/systemd/system/wg-provision.service`
* `systemctl daemon-reload`
* `systemctl enable wg-provision`
* `systemctl start wg-provision`

### Supervisord
* `mkdir -p /var/service/wg-provision`
* `ln -s /opt/wg-provision/current/init/wg-provision.supervisord.ini /etc/supervisor.d/wg-provision.ini`
* `service supervisord restart`

Configuration
=============

You can adjust configuration by modifying the `env` file

* `HOST` - Listening host (`localhost` by default)
* `PORT` - Listening port (`3000` by default)
* `VPN_INTERFACE` - Name of the network interface (`wg0` by default)
* `GATEWAY_INTERFACE` - Name of the internet-attached interface (autodetected)
* `DNS` - Domain name of the host (autodetected)
* `BYPASS` - Turns off authentication. Does not modify wireguard settings (not recommended)

Usage
=====

While connected to the VPN, hit the `/invites` endpoint. You'll be presented with a guid.

From the main page use the guid to obtain a configuration.

Pull requests welcome.
