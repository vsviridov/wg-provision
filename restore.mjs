#!/bin/env node

import fs from "fs/promises";
import { parse } from "ini";
import { resolve } from "path";

import { pubKey, addPeer } from "./wireguard.mjs";

const invitesPath = resolve(__dirname, "invites");
const claimedPath = resolve(invitesPath, "claimed");

async function main() {
  const invites = await fs.readdir(claimedPath);

  for (const path of invites) {
    const file = await fs.readFile(resolve(claimedPath, path), {
      encoding: "ascii",
    });

    const parsed = parse(file);

    if (!parsed.Interface) {
      continue;
    }

    const { PrivateKey, Address: FullAddress } = parsed.Interface;

    const [Address] = FullAddress.split("/");

    const publicKey = await pubKey(PrivateKey);

    const output = await addPeer("wg0", publicKey, Address);

    console.log(output);
  }
}

main().catch((ex) => {
  console.error(ex);
  process.exit(1);
});
