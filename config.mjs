import { networkInterfaces } from "os";
import ip from "ip";
import "dotenv/config";

const { isPrivate } = ip;
export const PORT = process.env.PORT || 3000;
export const VPN_INTERFACE = process.env.VPN_INTERFACE || "wg0";
export const PREFIX = process.env.PREFIX || "/wg/";

const interfaces = networkInterfaces();

export const autodetected = Object.entries(interfaces)
  .map(([iface, bindings]) => [
    iface,
    bindings.filter(
      (b) => !b.internal && b.mac !== "00:00:00:00:00:00" && b.family === "IPv4"
    ),
  ])
  .filter((a) => a[1].length)[0][0];

export const GATEWAY_INTERFACE =
  process.env.GATEWAY_INTERFACE || autodetected || "eth0";

if (!(VPN_INTERFACE in interfaces)) {
  console.log(
    `Interace "${VPN_INTERFACE}" does not seem to be set up. Was it configured correctly?`
  );
  process.exit(1);
}

function isPublicIpV4Interface(iface) {
  return iface.family === "IPv4" && !isPrivate(iface.address);
}

export const iface = interfaces[VPN_INTERFACE][0];

export const gateway = interfaces[GATEWAY_INTERFACE].find(isPublicIpV4Interface);

export const HOST = process.env.HOST || iface.address || "127.0.0.1";
