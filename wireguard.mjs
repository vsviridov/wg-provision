import { promisify } from "util";
import childProcess from "child_process";

const exec = promisify(childProcess.exec);
const execFile = promisify(childProcess.execFile);

export async function pubKey(privKey) {
  const { stdout } = await exec(`echo "${privKey}" | wg pubkey`);

  return stdout
    .split(/[\r\n]+/)
    .filter((x) => !!x)
    .join("");
}
export async function addPeer(iface, pubKey, address) {
  const { stdout } = await execFile("wg", [
    "set",
    iface,
    "peer",
    pubKey,
    "allowed-ips",
    `${address}/32`,
  ]);

  return stdout;
}
